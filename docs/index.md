---
author: Mireille Coilhac
title: 🏡 Accueil
---


# Ancien tutoriel pour l'enseignant qui crée son site basique


!!! danger "Attention : Migration à programmer de la forge AEIF vers la forge Education Nationale"

    La migration devra être réalisée avant le 30 juin 2024. [réaliser la migration et la redirection](migration/migrer.md){ .md-button target="_blank" rel="noopener" }   



## 👉 Le sites modèle

Ce tutoriel accompagne les modèles suivants à cloner, pour aider à leur prise en main :


[Rendu du site basique](https://docs.forge.apps.education.fr/modeles/mkdocs-simple-review/){ .md-button target="_blank" rel="noopener" }
[Dépôt du site basique à cloner](https://forge.apps.education.fr/docs/modeles/mkdocs-simple-review){ .md-button target="_blank" rel="noopener" } 

!!! danger "Attention : pour créer un site avec Python intégré"

    Pour créer un site avec Python, c'est ici :  [Tutoriel pour créer son site avec Python intégré](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/){:target="_blank" }


## 👉 Cloner le sites modèle pour les personnaliser

Vous pouvez faire une bifurcation (on dit aussi un «fork») de ces modèles pour réaliser le vôtre.  
Le tutoriel pour réaliser cette bifurcation est sur ce site, dans la rubrique "Comment créer un site à partir d'un autre".

Lien direct : [Faire un "fork"](08_tuto_fork/1_fork_projet.md){ .md-button target="_blank" rel="noopener" }


## 👉 Si vous débutez et pour comprendre la structure

Il est conseillé de commencer par : [Avant de démarrer](01_demarrage/1_demarrage.md/){ .md-button target="_blank" rel="noopener" }


!!! abstract "En bref"

    😀 Vous pourrez facilement recopier toutes les syntaxes depuis les différents tutoriels proposés.

    * Le dépôt de ce tuto est ici : [Dépôt du tutoriel](https://forge.apps.education.fr/docs/tutoriels/tutoriel-site-simple){ .md-button target="_blank" rel="noopener" }



_Dernière mise à jour le 19/06/2024_
